package com.nulp.bikeclub;

import com.nulp.bikeclub.model.bike.*;
import com.nulp.bikeclub.model.event.Event;
import com.nulp.bikeclub.model.event.EventAttendee;
import com.nulp.bikeclub.model.event.EventType;
import com.nulp.bikeclub.model.event.Schedule;
import com.nulp.bikeclub.model.location.City;
import com.nulp.bikeclub.model.person.Employee;
import com.nulp.bikeclub.model.person.Member;
import com.nulp.bikeclub.model.person.Role;
import com.nulp.bikeclub.model.person.SkillLevel;
import com.nulp.bikeclub.model.rental.BikeStorage;
import com.nulp.bikeclub.model.subscription.Subscription;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@SpringBootApplication
@EnableJpaAuditing
public class BikeclubApplication {

    public static void main(String[] args) {
        SpringApplication.run(BikeclubApplication.class, args);
    }

    @Bean
    public Docket apiV1() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
            }
        };
    }

    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer()
    {
        return RepositoryRestConfigurer.withConfig(config -> {
            config.exposeIdsFor(City.class);
            config.exposeIdsFor(SkillLevel.class);
            config.exposeIdsFor(Role.class);
            config.exposeIdsFor(BikeType.class);
            config.exposeIdsFor(BrakeType.class);
            config.exposeIdsFor(BikeBrand.class);
            config.exposeIdsFor(Groupset.class);
            config.exposeIdsFor(Wheelset.class);
            config.exposeIdsFor(Employee.class);
            config.exposeIdsFor(Member.class);
            config.exposeIdsFor(Event.class);
            config.exposeIdsFor(EventAttendee.class);
            config.exposeIdsFor(EventType.class);
            config.exposeIdsFor(Schedule.class);
            config.exposeIdsFor(BikeStorage.class);
            config.exposeIdsFor(Subscription.class);
        });
    }
}
