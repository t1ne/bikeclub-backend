package com.nulp.bikeclub.repository.subscription;

import com.nulp.bikeclub.model.subscription.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource
public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {
}
