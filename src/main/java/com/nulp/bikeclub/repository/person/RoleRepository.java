package com.nulp.bikeclub.repository.person;

import com.nulp.bikeclub.model.person.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface RoleRepository extends CrudRepository<Role, Long> {
}
