package com.nulp.bikeclub.repository.person;

import com.nulp.bikeclub.model.person.SkillLevel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface SkillLevelRepository extends CrudRepository<SkillLevel, Long> {
}
