package com.nulp.bikeclub.repository.person;

import com.nulp.bikeclub.model.person.Member;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource
public interface MemberRepository extends CrudRepository<Member, Long> {
}
