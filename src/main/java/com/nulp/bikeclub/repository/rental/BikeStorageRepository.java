package com.nulp.bikeclub.repository.rental;

import com.nulp.bikeclub.model.rental.BikeStorage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface BikeStorageRepository extends CrudRepository<BikeStorage, Long> {
}
