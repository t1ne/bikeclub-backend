package com.nulp.bikeclub.repository.rental;

import com.nulp.bikeclub.model.rental.Rental;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource
public interface RentalRepository extends CrudRepository<Rental, Long> {
}
