package com.nulp.bikeclub.repository.bike;

import com.nulp.bikeclub.model.bike.BikeBrand;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface BrandRepository extends CrudRepository<BikeBrand, Long> {
}
