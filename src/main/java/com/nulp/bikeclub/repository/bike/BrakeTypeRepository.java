package com.nulp.bikeclub.repository.bike;

import com.nulp.bikeclub.model.bike.BrakeType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface BrakeTypeRepository extends CrudRepository<BrakeType, Long> {
}
