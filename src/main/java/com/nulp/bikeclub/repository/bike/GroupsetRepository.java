package com.nulp.bikeclub.repository.bike;

import com.nulp.bikeclub.model.bike.Groupset;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface GroupsetRepository extends CrudRepository<Groupset, Long> {
}
