package com.nulp.bikeclub.repository.bike;

import com.nulp.bikeclub.model.bike.BikeType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface BikeTypeRepository extends CrudRepository<BikeType, Long> {
}
