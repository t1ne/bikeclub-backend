package com.nulp.bikeclub.repository.bike;

import com.nulp.bikeclub.model.bike.Wheelset;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface WheelsetRepository extends CrudRepository<Wheelset, Long> {
}
