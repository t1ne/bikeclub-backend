package com.nulp.bikeclub.repository.bike;


import com.nulp.bikeclub.model.bike.Bike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource
public interface BikeRepository extends CrudRepository<Bike, Long> {
}
