package com.nulp.bikeclub.repository.event;

import com.nulp.bikeclub.model.event.Event;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface EventRepository extends CrudRepository<Event, Long> {
}
