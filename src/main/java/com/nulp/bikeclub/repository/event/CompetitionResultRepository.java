package com.nulp.bikeclub.repository.event;

import com.nulp.bikeclub.model.event.CompetitionResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource
public interface CompetitionResultRepository extends CrudRepository<CompetitionResult, Long> {
}
