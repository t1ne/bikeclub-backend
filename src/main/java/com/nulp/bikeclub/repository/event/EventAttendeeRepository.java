package com.nulp.bikeclub.repository.event;

import com.nulp.bikeclub.model.event.EventAttendee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface EventAttendeeRepository extends CrudRepository<EventAttendee, Long> {
}
