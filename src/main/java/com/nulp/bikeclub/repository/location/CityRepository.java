package com.nulp.bikeclub.repository.location;

import com.nulp.bikeclub.model.location.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface CityRepository extends CrudRepository<City, Long> {
}
