package com.nulp.bikeclub.repository.location;

import com.nulp.bikeclub.model.location.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface DepartmentRepository extends CrudRepository<Department, Long> {
}
