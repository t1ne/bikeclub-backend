package com.nulp.bikeclub.model.subscription;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nulp.bikeclub.model.CreatableUpdatableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Subscription extends CreatableUpdatableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subscription_id")
    private Long id;

    private String name;

    private double price;

    @Column(name = "duration_in_month")
    private int durationInMonths;
}
