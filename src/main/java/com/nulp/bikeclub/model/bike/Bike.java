package com.nulp.bikeclub.model.bike;

import com.nulp.bikeclub.model.CreatableUpdatableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Bike extends CreatableUpdatableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bike_id")
    private Long id;

    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "brand_id", referencedColumnName = "brand_id")
    private BikeBrand brand;

    private String model;

    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "type_id", referencedColumnName = "bike_type_id")
    private BikeType type;

    @Column(name = "frame_size")
    private int frameSize;

    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "brake_type_id", referencedColumnName = "brake_type_id")
    private BrakeType brakeType;

    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "groupset_id", referencedColumnName = "groupset_id")
    private Groupset groupset;

    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "wheelset_id", referencedColumnName = "wheelset_id")
    private Wheelset wheelset;

    private double weight;

    @Column(name = "release_year")
    private int releaseYear;

    @Column(name = "price_per_hour")
    private double pricePerHour;
}
