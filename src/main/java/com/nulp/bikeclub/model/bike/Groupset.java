package com.nulp.bikeclub.model.bike;

import com.nulp.bikeclub.model.CreatableUpdatableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Groupset extends CreatableUpdatableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "groupset_id")
    private Long id;

    private String name;

    private String description;

    @Column(name = "front_gear")
    private int frontGearsCount;

    @Column(name = "rear_gear")
    private int rearGearsCount;
}
