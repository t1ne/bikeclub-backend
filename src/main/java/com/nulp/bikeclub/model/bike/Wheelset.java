package com.nulp.bikeclub.model.bike;

import com.nulp.bikeclub.model.CreatableUpdatableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Wheelset extends CreatableUpdatableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wheelset_id")
    private Long id;

    private String name;

    private int size;

    private int width;
}
