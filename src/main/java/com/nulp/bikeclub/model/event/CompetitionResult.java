package com.nulp.bikeclub.model.event;

import com.nulp.bikeclub.model.CreatableUpdatableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDateTime;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class CompetitionResult extends CreatableUpdatableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "result_id")
    private Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "event_attendee_id", referencedColumnName = "event_attendee_id")
    private EventAttendee eventAttendee;

    private Time time;

    private int place;
}
