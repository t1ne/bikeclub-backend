package com.nulp.bikeclub.model.event;

import com.nulp.bikeclub.model.CreatableUpdatableEntity;
import com.nulp.bikeclub.model.person.Employee;
import com.nulp.bikeclub.model.person.Member;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Schedule extends CreatableUpdatableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedule_id")
    private Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "event_id", referencedColumnName = "event_id")
    private Event event;

    private Date date;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "organizer_id", referencedColumnName = "employee_id")
    private Employee organizer;

    @ManyToMany
    @JoinTable(
            name = "EventAttendees",
            joinColumns = @JoinColumn(name = "schedule_entry_id"),
            inverseJoinColumns = @JoinColumn(name = "member_id"))
    private List<Member> attendees;
}
