package com.nulp.bikeclub.model.event;

import com.nulp.bikeclub.model.CreatableUpdatableEntity;
import com.nulp.bikeclub.model.person.Member;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@Table(name = "EventAttendees")
public class EventAttendee extends CreatableUpdatableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_attendee_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "schedule_entry_id")
    private Schedule scheduleEntry;

    @ManyToOne
    @JoinColumn(name = "member_id")
    private Member member;
}
