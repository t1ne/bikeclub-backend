package com.nulp.bikeclub.model.rental;

import com.nulp.bikeclub.model.CreatableUpdatableEntity;
import com.nulp.bikeclub.model.bike.Bike;
import com.nulp.bikeclub.model.location.Department;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class BikeStorage extends CreatableUpdatableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bike_storage_id")
    private Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "bike_id", referencedColumnName = "bike_id")
    private Bike bike;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id", referencedColumnName = "department_id")
    private Department department;

    private int count;
}
