package com.nulp.bikeclub.model.rental;

import com.nulp.bikeclub.model.CreatableUpdatableEntity;
import com.nulp.bikeclub.model.person.Employee;
import com.nulp.bikeclub.model.person.Member;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Rental extends CreatableUpdatableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rental_id")
    private Long id;

    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "bike_id", referencedColumnName = "bike_storage_id")
    private BikeStorage bikeAtStorage;

    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "member_id", referencedColumnName = "member_id")
    private Member member;

    @Column(name = "duration_in_hours")
    private int durationInHours;

    @Column(name = "total_price")
    private double totalPrice;

    @Column(name = "return_time")
    private LocalDateTime returnTime;

    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "verified_employee_id", referencedColumnName = "employee_id")
    private Employee verifiedEmployee;
}
