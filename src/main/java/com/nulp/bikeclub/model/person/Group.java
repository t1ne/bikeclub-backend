package com.nulp.bikeclub.model.person;

import com.nulp.bikeclub.model.CreatableUpdatableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@Table(name = "`Group`")
public class Group extends CreatableUpdatableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id")
    private Long id;

    private String name;

    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "skill_level_id", referencedColumnName = "skill_level_id")
    private SkillLevel skillLevel;

    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "trainer_id", referencedColumnName = "employee_id")
    private Employee trainer;

    @ManyToMany
    @JoinTable(
            name = "GroupMembers",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "member_id"))
    private List<Member> members;
}
