package com.nulp.bikeclub.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class CreatableUpdatableEntity {
    @CreatedDate
    @Column(name = "create_time")
    private Date createdAt;

    @LastModifiedDate
    @Column(name = "update_time")
    private Date updatedAt;
}


